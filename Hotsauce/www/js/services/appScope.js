define(['./module'], function(services){
   'use strict';
    services.service('appScope', [function(){
        var scope = {
            musicPlaying:false,
            loggedIn:false,
            service:'soundcloud',
            serviceCode:2,
            spotifyToken:{},
            soundcloudToken:{},
            eightTracksToken:'',
            screenValues:{}
        };
        return{
            add:function(key, value){
                scope[key] = value;
            },

            /*
             * Options = {
             *  key: <optional>,
             *  value: <required> Can set just the key or full parent value.
             * }
             */
            set:function(parent, options){
                if(options){
                    if(options.key && scope[parent]){
                        scope[parent][options.key] = options.value;
                    }else{
                        scope[parent] = options.value;
                    }
                }
            },

            setScope:function(s){
                scope = s;
            },

            get:function(key){
                return scope[key]? scope[key] : false;
            },

            getScope:function(){
                return scope;
            }

            //TODO Ask dataService for Tokens stored in sqlLite db.
        }
    }]);
});