define(['./module'], function(services){
   'use strict';
    services.service('dataService',['tokenDatabase', 'apputils', function(database, apputils){
        var __values = {};
        return{
            get:function(id, variable){
                if(!__values[id]){
                    __values[id] = {};
                }
                return (typeof __values[id][variable] !== undefined)? __values[id][variable] : false;
            },

            set:function(id, key, value){
                if(!__values[id]){
                    __values[id] = {};
                }
                __values[id][key] = value;
            },

            getAccessToken:function(id){
                return database.select(id, 'token');
            },

            setAccessToken:function(id, tokenObj){
                //TODO Update the value or Insert the value.... FUCK.
            },

            /*
            * We need a generic user profile here so all screens can be reused.
             */
            createUserProfile:function(id, userData){
                var user = {
                    name: '',
                    image: '',
                    email:'',
                    source:userData
                };

                if(id === 'spotify'){
                    if(userData.display_name){
                        user.name = userData.display_name;
                    }else{
                        user.name = userData.id;
                    }
                    if(userData.images.length > 0){
                        user.image = userData.images[0].url != null? userData.images[0].url : 'img/hunter.jpg';
                    }else{
                        user.image = 'img/hunter.jpg'
                    }
                    user.email = userData.email;
                }else if( id === 'soundcloud'){
                    user.name = userData.full_name;
                    if(userData.avatar_url){
                        user.image = userData.avatar_url;
                    }else{
                        user.image = 'img/hunter.jpg';
                    }//Maybe a default image down the road i dont fucking know.
                    user.email = userData.username;
                }

                return user;
            },

            createTrackInformation:function(id, trackData){
                var info = {
                    tracks:[]
                };

                if(id === 'spotify'){//TODO Make a spotify Constant
                    //TODO Massage Spotify data.
                }else if(id === 'soundcloud'){//TODO make a soundcloud constant.
                    angular.forEach(trackData, function(t){
                        var track = {
                            name: t.title,
                            artist:null,
                            album:null,
                            artist_img:null,
                            album_img:null,
                            stream:t.stream_url,
                            image:t.artwork_url? t.artwork_url : t.user.avatar_url,
                            duration: apputils.getDuration(t.duration),
                            description: t.description,
                            source:t
                        };
                        info.tracks.push(track);
                    });
                }

                return info;
            }
        }
    }]);
});