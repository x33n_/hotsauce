define(['./module'], function(controllers){
    'use strict';
    controllers.controller('playlistController',
        [
            '$scope',
            '$rootScope',
            'musicPlayer',
            'navigator',
            'spotifyData',
            'spotify',
            function(scope, $rootScope, musicPlayer, navigator, spotifyData, services){
                scope.cPlaylist = {};
                scope.content = {};
                scope.title = '';
                scope.tokenObject = {};
                scope.loaded = false;

                scope.init = function(){
                    scope.cPlaylist = spotifyData.get('currentPlaylist');
                    scope.tokenObject = spotifyData.get('tokenData');

                    if(scope.cPlaylist){
                        scope.title = scope.cPlaylist.name;
                        scope.getTracks();
                    }
                };

                scope.getTracks = function(){
                    var url = scope.cPlaylist.href;
                    var headers = {
                        Authorization:'Bearer '+scope.tokenObject.access_token
                    };

                    services.requestUrl(url, {}, headers, 'GET').then(function(data){
                        console.log('Successfully Retrieved tracks. Response: ');
                        scope.content = data;
                        scope.loaded = true;
                    });
                };

                scope.goBack= function(){
                    navigator.goBack();
                };

                scope.playTrack = function($index){

                    var tracks = [];
                    angular.forEach(scope.content.items, function(item){
                        var obj = {
                            url: item.track.preview_url,
                            info:{
                                name: item.track.name,
                                artist: item.track.artists[0].name,
                                album: item.track.album.name,
                                img: item.track.album.images[0].url
                            }
                        }
                        tracks.push(obj);
                    });

                    navigator.navigate('/play');

                    $rootScope.$broadcast('audio.set', tracks);
                };


                scope.init();
            }
        ]
    );
});