define(['./module'], function(controllers){
    'use strict';
    controllers.controller('searchController',
        [
            'soundcloudApi',
            'dataService',
            '$scope',
            'appScope',
            'navigator',
            function(sc, cisumData, scope, appScope, navigator){
                scope.appScope = appScope.getScope();
                scope.searchPage = {
                    id: scope.appScope.service,
                    pageValues: {
                        loaded:false
                    },
                    playerVisible:false,
                    init:function(){
                        var cache = scope.appScope.screenValues.searchPage;
                        if(cache){
                            scope.searchPage.pageValues = cache.values;
                        }
                        scope.searchPage.playerVisible = scope.appScope.musicPlaying;
                        console.log('Music Playing: ' + scope.searchPage.playerVisible);
                    },

                    search:function(){
                        if(scope.searchPage.pageValues.query){
                            if(scope.searchPage.id === 'soundcloud'){
                                var params = {
                                    q:scope.searchPage.pageValues.query,
                                    oauth_token:scope.appScope.soundcloudToken.access_token
                                };
                                scope.searchPage.pageValues.loaded = false;
                                scope.searchPage.pageValues.results = {};
                                sc.request('tracks', params, {}, 'GET').then(function(data){
                                    console.log('Successful Search');
                                    scope.searchPage.pageValues.results = cisumData.createTrackInformation('soundcloud', data);
                                    scope.searchPage.pageValues.loaded = true;
                                });
                            }
                        }else{
                            //TODO
                        }
                    },

                    playSong:function(i) {
                        var track ={
                            tracks:scope.searchPage.pageValues.results.tracks,
                            auth:'?oauth_token='+scope.appScope.soundcloudToken.access_token,
                            currentTrack: i,
                            total:scope.searchPage.pageValues.results.tracks.length
                        };

                        appScope.add('music', track);
                        scope.searchPage.storeScope();
                        navigator.navigate('/play');
                    },

                    storeScope:function(){
                        scope.appScope.screenValues.searchPage = {
                            values: scope.searchPage.pageValues
                        };
                        appScope.setScope(scope.appScope);
                    }
                };

                scope.searchPage.init();
            }
        ]);
});