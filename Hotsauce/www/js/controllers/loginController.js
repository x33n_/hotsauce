/**
 * Created by benjamin on 7/17/14.
 */
define(['./module'],
    function (controllers) {
        "use strict";
        controllers.controller('loginController',
            [
                'navigator',
                '$scope',
                'appScope',
                'spotify',
                'eightTracks',
                'apputils',
                'dataService',
                'dialogService',
                'soundcloudApi',
                function (navigator, scope, appScope, spotify, eightTracks, apputils, cisumData , dialog, sc) {

                    scope.applicationScope = {};
                    scope.loginPage = {
                        values:{},
                        eightTracks:false,
                        init:function(){
                            scope.applicationScope = appScope.getScope();
                        },

                        login:function(){
                            //The following will be a reference table for services.
                            // 1 - <Default> Spotify
                            // 2 - Soundcloud
                            // 3 - HypeM
                            // 4 - 8Tracks
                            // More to be added later.
                            scope.applicationScope = appScope.getScope();
                            var serviceCode = scope.applicationScope.serviceCode;

                            switch(serviceCode){
                                case 1:
                                    scope.applicationScope.service = 'spotify';
                                    spotifyLogin();
                                    break;
                                case 2:
                                    scope.applicationScope.service = 'soundcloud';
                                    soundCloudLogin();
                                    break;
                                case 3:
                                    dialog.showModal({},
                                        {
                                            title: 'Not Implemented',
                                            message:'HyepM not implemented :(',
                                            closeButtonAvailable: false
                                        });
                                    //HypeM Login();
                                    break;
                                case 4:
                                    //8tracks
                                    /*
                                    dialog.showModal({},
                                        {
                                            title: 'Not Implemented',
                                            message:'8Tracks not implemented :(',
                                            closeButtonAvailable: false
                                        });
                                        */
                                    eightTracksLogin();
                                    break;
                                default:
                                    console.log('Do nothing.');
                            }
                        },

                        updateAppScope:function(){
                            appScope.setScope(scope.applicationScope);
                        }
                    };

                    //Spotify Login method
                    var spotifyLogin = function(){
                        spotify.login().then(function (data) {
                            console.log("loginController received Spotify Access Token");
                            var token = data;
                            token.extend('expireTime', (new Date().getTime() / 1000 + token.expires_in));
                            scope.applicationScope.spotifyToken = token;
                            cisumData.set('spotify', 'token', token); //TODO Hardcode spotify from constants.
                            if(token.access_token){
                                spotify.getUserProfile(token.access_token).then(function(data){
                                    console.log('User profile received');
                                    cisumData.set('spotify', "userProfile", data); //TODO Hardcode spotify from constants.
                                    //Go to profile.
                                    scope.applicationScope.loggedIn = true;
                                    scope.loginPage.updateAppScope();
                                    navigator.navigate('/profile');
                                });
                            }
                        }, function (err) {
                            console.log('No Token Received Error' + err);
                            dialog.showModal({},
                                {
                                    title:'Error',
                                    message:'Could not connect to service.',
                                    closeButtonAvailable: false
                                });
                        });
                    };

                    //Soundcloud login
                    var soundCloudLogin = function(){
                        sc.login().then(function(data){
                            console.log('Soundcloud Access Token Retrieved DATA: ' + JSON.stringify(data));
                            var token = apputils.queryStringToJson(data[1]);
                            cisumData.set('soundcloud', 'token', token); //TODO Hardcode soundcloud in constants.
                            scope.applicationScope.soundcloudToken = token;
                            if(token.access_token){
                                sc.request('me.json',{"oauth_token":token.access_token}, {}, 'GET').then(function(data){
                                    console.log('User Profile Retrieved from soundcloud! ' + JSON.stringify(data));
                                    cisumData.set('soundcloud','userProfile', data);
                                    scope.applicationScope.loggedIn = true;
                                    scope.loginPage.updateAppScope();
                                    navigator.navigate('/profile');
                                });
                            }
                        });
                    };

                    //8Tracks login
                    var eightTracksLogin = function(){
                        if(scope.loginPage.values.username && scope.loginPage.values.password){
                            eightTracks.login_web().then(function(response){
                                console.log('8Tracks login successfully: ' + JSON.stringify(response));
                            });
                            /*eightTracks.mix_sets('all', {include:'mixes'}).then(function(response){
                                console.log('Returned: ' + JSON.stringify(response));
                            });*/
                        }else{
                            dialog.showModal({},
                                {
                                    title:'Invalid Login',
                                    message:'Please input username and password'
                                });
                        }
                    };

                    scope.$on('menu.eightTracks', function(event, args){
                        scope.loginPage.eightTracks = args;
                    });

                    scope.loginPage.init();
                }
            ]
        );
    });