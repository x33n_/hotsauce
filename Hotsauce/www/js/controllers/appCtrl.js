/**
 * Created by benjamin on 6/10/14.
 */
define(['./module'
], function(controllers){
    'use strict';
    controllers.controller('appCtrl', ['$scope', '$rootScope', '$location', '$sce', 'spotify', function($scope, $rootScope, $location, $sce, spotify){
        $scope.service = 'spotify';
        $scope.query = "";
        $scope.data = [];
        $scope.loadedAlbum = {};
        $scope.tokenData = [];
        $scope.token = '';
        $scope.userId = '';

        $scope.changeService = function(e){
            console.log(e);
        };

        $scope.getDuration = function(milliseconds){
            var ms = milliseconds,
                min = (ms/1000/60) << 0,
                sec = (ms/1000) % 60;

            return min + ":" + sec.toFixed(2);
        };

        $scope.setIFrame = function(uri){
            var url = 'https://embed.spotify.com/?' + uri;
            return $sce.trustAsResourceUrl(url);
        };

        //Watchers
        $scope.$watch('query', function(nv){
            $scope.query = nv;
        });
        //End Watchers

        //Button clicks
        $scope.login = function(){

            spotify.spotifyLogin().then(function(data){
                console.log("App Control Received Token");
                $scope.tokenData = data;
                $scope.token = data[1].substring(0, data[1].indexOf('&'));
                $scope.getUserProfile($scope.token);
            }, function(err){
                console.log("Something went wrong getting token... " + err);
            });
        };

        $scope.getUserProfile = function(token){
            var method = 'me';
            var headers = {
                Authorization: 'Bearer '+token
            };
            spotify.request(method, {}, headers, 'GET').then(function(data){
                console.log("User Profile:  " + JSON.stringify(data));
            }, function(err){
                console.log("Something went wrong getting user profile.");
            });
        };

        $scope.go = function(path){
            $location.path(path);
        };

        $scope.search = function(){
            if($scope.query !== ""){
                spotify.request('search', {q: $scope.query, type:'artist,album,track'}).then(function(result){
                    if(result){
                        if(result.artists){
                            $scope.data.artists = result.artists.items;
                        }
                        if(result.albums){
                            $scope.data.albums = result.albums.items;
                        }
                        if(result.tracks){
                            $scope.data.tracks = result.tracks.items;
                        }
                    }
                    $scope.loaded = true;
                });
            }
        };

        $scope.openAlbum = function(index){
            $scope.loadedAlbum = $scope.data.albums[index];
            $scope.go('album');
        };

        $scope.openSong = function(index){
            window.open($scope.data.tracks[index].href);
        };
        //End Buttons clicks
    }]);
});
