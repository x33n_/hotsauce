/**
 * Created by benjamin on 7/17/14.
 */
define(['./module'], function(controllers){
    'use strict';
    controllers.controller('profileController',
        [
            '$scope',
            'appScope',
            'navigator',
            'spotify',
            'soundcloudApi',
            'dataService',
            function(scope, appScope, navigator, spotify, sc, cisumData){

                scope.applicationScope = appScope.getScope();
                scope.profilePage = {
                    values:{
                        userProfile:{},
                        displayInfo:{}
                    },
                    loaded:false,
                    id: scope.applicationScope.service,

                    init:function(){
                        if(scope.applicationScope.screenValues.profilePage){
                            scope.profilePage.values = scope.applicationScope.screenValues.profilePage.values;
                        }else {
                            var rawUser = cisumData.get(scope.profilePage.id, 'userProfile');
                            scope.profilePage.values.userProfile = cisumData.createUserProfile(scope.profilePage.id, rawUser);
                        }

                        if (scope.profilePage.id === 'soundcloud') {
                            scope.profilePage.getSoundCloudFavorites();
                        } else if (scope.profilePage.id === 'spotify') {
                            //TODO Make a method to get spotify playlist info for default.
                        }
                    },

                    playSong:function(i){
                        var track ={
                            tracks:scope.profilePage.values.displayInfo.info.tracks,
                            auth:'?oauth_token='+scope.applicationScope.soundcloudToken.access_token,
                            currentTrack: i,
                            total:scope.profilePage.values.displayInfo.info.tracks.length
                        };

                        appScope.add('music', track);
                        scope.profilePage.updateScope();
                        navigator.navigate('/play');
                    },

                    getSpotifyPlaylist:function(){
                        //TODO
                    },

                    getSoundCloudFavorites:function(){
                        var at = scope.applicationScope.soundcloudToken.access_token,
                            user_id = scope.profilePage.values.userProfile.source.id,
                            path = 'users/'+user_id+'/favorites.json';

                        sc.request(path, {oauth_token:at},{},'GET').then(function(data){
                            cisumData.set('soundcloud', 'userFavorites', data);
                            scope.profilePage.values.displayInfo.info = cisumData.createTrackInformation('soundcloud', data);
                            scope.profilePage.loaded = true;
                        });
                    },

                    //CALL BEFORE NAVIGATING
                    updateScope:function(){
                        scope.applicationScope.screenValues.profilePage = {
                            values: scope.profilePage.values
                        };
                        appScope.setScope(scope.applicationScope);
                    }
                };

                scope.profilePage.init();
            }
        ]
    );
});