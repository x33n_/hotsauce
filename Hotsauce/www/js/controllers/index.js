/**
 * Created by benjamin on 6/10/14.
 */
define([
    './appCtrl',
    './loginController',
    './profileController',
    './playlistController',
    './searchController'
], function(){});
