define(['./module'],function(factories){
    'use strict';
    factories.factory('tokenDatabase', ['$q', function($q){
        var database = 'cisum.db';
        var table_name = 'tokens';
        var table_vals = '(id varchar(20), token varchar(45), expiration int)';

        var connect = function(sql){
            var d = $q.defer();

            if(window.sqlitePlugin){
                var db = window.sqlitePlugin.openDatabase({name: database});

                db.transaction(function(tx){

                    tx.executeSql('CREATE TABLE IF NOT EXISTS ' + table_name + table_vals);
                    tx.executeSql(sql, [], function(res){
                        console.log('databaseManager:  Success');
                        d.resolve(res);
                    });
                }, function(e){
                    console.log('databaseManager ERROR: ' + JSON.stringify(e));
                    d.reject(e);
                });
            }else{
                console.log('No Sql Lite plugin.');
                d.reject('No Plugin');
            }

            return d.promise;
        };

        var clearTable = function(){
            if(window.sqlitePlugin){
                var db = window.sqlitePlugin.openDatabase({name:database});

                db.transaction(function(tx){
                    tx.executeSql('DROP TABLE IF EXISTS ' + table_name);
                });
            }
        };

        return{
            select:function(id , column){
                var d = $q.defer();
                var sql;
                if(column){
                    sql = 'SELECT ' + column + ' FROM ' + table_name + ' WHERE id =' + id +';';
                }else{
                    sql = 'SELECT * FROM ' + table_name + ' WHERE id =' + id +';';
                }
                console.log(sql);
                connect(sql).then(function(res){
                    d.resolve(res);
                });

                return d.promise;
            },

            insert:function(column, value){
                var sql = 'INSERT INTO ' + table_name + ' (';
                if(column == typeof Array){
                    angular.forEach(column, function(c){
                        sql += c + ',';
                    });
                    sql = sql.substring(0, sql.length - 1);
                }else{
                    sql += column;
                }
                sql += ') VALUES (';
                if(value == typeof Array){
                    angular.forEach(value, function(v){
                        sql += v + ',';
                    });
                    sql = sql.substring(0, sql.length - 1);
                }else{
                    sql += value;
                }
                sql += ');';

                //Don't really care if this returns since we don't need
                //Anything back from an insert.
                console.log(sql);
                connect(sql).then(function(res){
                    console.log(JSON.stringify(res));
                });
            },

            update:function(id, column, value){
                var sql = 'UPDATE ' + table_name + ' SET ';
                if(column == typeof Array && (value.length === column.length)){
                    for(var i = 0; i < column.length;i++){
                        sql += column[i]+'='+value[i]+',';
                    }
                    sql = sql.substring(0, sql.length -1);
                }else{
                    sql += column + '=' + value;
                }

                sql += ' WHERE id=' + id + ';';
                console.log(sql);
                connect(sql).then(function(res){
                    console.log(JSON.stringify(res));
                });
            },

            dropTable:function(){
                clearTable();
            }
        };
    }]);
});