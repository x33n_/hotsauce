define(['./module'], function(factories){
    'use strict';
    factories.factory('navigator', ['$rootScope', '$location', 'dataService', function(scope, location, cisumData){
        return{
            /*
             * Navigator to take browser to the specified path and trigger the emit event
             * To pass data between controllers.
             */
            navigate:function(route, event){
                var map = cisumData.get('navigation','map');
                if(map){
                    map.push(location.path());
                    cisumData.set('navigation','map', map);
                }else{
                    var map = [location.path()];
                    cisumData.set('navigation','map', map);
                }
                location.path(route);
                if(event){
                    //TODO maybe events?
                }
            },

            /*
            * Give some very simple go back functionality to the app.
             */
            goBack:function(){
                var map = cisumData.get('navigation', 'map');
                if(map){
                    location.path(map.pop());
                }
            },

            getCurrentView:function(){
                return location.path();
            }
        }
    }]);
});