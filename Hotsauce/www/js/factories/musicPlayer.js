/**
 * Created by benjamin on 7/18/14.
 */
define(['./module'], function(factories){
    'use strict';
    factories.factory('musicPlayer', ['$rootScope','appScope', function(rootScope, appScope){
        var isPlaying = false;
        var currentPlayer = {};
        var mediaTimer = null;
        var currentName = '';

        rootScope.audioPosition = '';

        return{
            playing:function(){
              return isPlaying;
            },

            play:function(src, success, fail, name){
                if(isPlaying && currentPlayer){
                    currentPlayer.stop();
                    currentPlayer = {};
                }else if(currentPlayer == typeof Media){
                    currentPlayer.stop();
                }
                currentName = name;
                currentPlayer = new Media(src, success, fail);
                currentPlayer.play();
                isPlaying = true;
                appScope.set('musicPlaying', {value:isPlaying});

                if(mediaTimer == null){
                    var that = this;
                    mediaTimer = setInterval(function(){
                        currentPlayer.getCurrentPosition(
                            function(position){
                                if(position > -1){
                                    that.setAudioPosition((position));
                                }
                            },

                            function(e){
                                console.log('Media player broke :(');
                                that.setAudioPosition("Error: " + e);
                            }
                        );
                    }, 1000);
                }
            },

            resume:function(){
                if(currentPlayer && !isPlaying){
                    currentPlayer.play();
                    isPlaying = true;
                }
            },

            pause:function(){
                if(currentPlayer){
                    currentPlayer.pause();
                    isPlaying = false;
                }
            },

            stop:function(){
                if(currentPlayer){
                    currentPlayer.stop();
                    isPlaying = false;
                    appScope.set('musicPlaying', {value:false});
                }
                clearInterval(mediaTimer);
                mediaTimer = null;
            },

            setAudioPosition:function(position){
                //console.log('Music Player Current Position: ' + position);
                rootScope.audioPosition = position;
            },

            getCurrentSrc:function(){
                return currentName;
            },

            getAudioPosition:function(){
                var time = parseInt(rootScope.audioPosition),
                    min = Math.floor(time / 60),
                    seconds = time - min * 60;
                isNaN(min)? min = 0: false;
                isNaN(seconds)? seconds = 0:false;

                return min + ':' + ("0"+seconds).slice(-2);
            }
        }
    }]);
});
