/**
 * Created by benjamin on 7/17/14.
 */
define(['./module'], function(factories){
    'use strict';
    factories.factory('apputils',  [function(){
        return{
            queryStringToJson:function(query){
                var pairs = query.split('&');
                var result = {};
                angular.forEach(pairs, function(value){
                    console.log('util queryToJson:  Value: '+ value);
                    if(value.indexOf('=') !== -1){
                        var pair = value.split('=');
                        result[pair[0]] = decodeURIComponent(pair[1] || '');
                    }else{
                        //This is purely for the access_token object.
                        //Since the token is returned without the key we know that
                        //No '=' in the value means we have the token.
                        //Else break the key value up as normal.
                        result.access_token = value;
                    }
                });

                //Look at whats been made.
                console.log('Created JSON object Successfully: ' + JSON.stringify(result));

                //Finally convert it to an actual JSON Object.
                return JSON.parse(JSON.stringify(result));
            },

            generateRandomString:function(length){
                var text = '';
                var possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

                for (var i = 0; i < length; i++) {
                    text += possible.charAt(Math.floor(Math.random() * possible.length));
                }
                return text;
            },

            /*
            * Modified to follow suit with angular js encode method.
             */
            serialize:function(obj){
                var str = [];
                for(var p in obj)
                    if (obj.hasOwnProperty(p)) {
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                            /*replace(/%40/gi, '@').
                            replace(/%3A/gi, ':').
                            replace(/%24/g, '$').
                            replace(/%2C/gi, ','));*/
                    }
                return str.join("&");
            },

            getDuration:function(ms){
                var min = (ms/1000/60) << 0,
                    sec = (ms/1000) % 60;

                return min + ":" + sec.toFixed(2);
            }
        }
    }]);
});
