/**
 * Created by benjamin on 7/17/14.
 */
define(['angular'], function(ng){
    'use strict';
    return ng.module('app.factories', []);
});