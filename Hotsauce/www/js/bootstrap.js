/**
 * Created by benjamin on 6/10/14.
 */
define([
    'require',
    'angular',
    'app',
    'routes'
], function(require, ng){
    'use strict';

    require(['domReady!'], function(document){
        ng.bootstrap(document, ['app']);
    });
});
