define(['./module'], function(apis){
    'use strict';

    apis.factory('spotify', ['$http', '$q', 'apputils', function($http, $q, apputils){
        var endpoint = {
            path:"https://api.spotify.com/v1/"
        };

        var redirect_uri = "http://localhost";
        var grant_type = "authorization_code";
        var spotify_scope = 'user-read-private user-read-email playlist-modify-public playlist-modify-private playlist-read-private';

        //Should be moved to a server
        var client_id = "83e8cfa131a949aeb39e311dea5bc505";
        var client_secret = "ae0815491a464e389612d4d7a78997b3";

        return{
            /*
            ************************************
            *Main request for spotify API.
            ************* METHODS **************
            * @path: search
            * @param: q <required> Search's query words
            * @param: type <required> the following are accepted
            *  - artist
            *  - album
            *  - track
            *  OR combinations I.E. artist, album, track
            **************************************
            *
             */
            request:function(method, params, headers, type){
                var schlep = $q.defer();

                var url = this.buildGetUrl(method);
                $http({
                    url: url,
                    params: params,
                    headers: headers,
                    method: type || 'GET'
                }).success(function(data){
                    schlep.resolve(data);
                });
                return schlep.promise;
            },

            requestUrl:function(url, params, headers, type){
                var schlep = $q.defer();

                $http({
                    url:url,
                    params:params,
                    headers:headers,
                    type:type || 'GET'
                }).success(function(data){
                    schlep.resolve(data);
                });
                return schlep.promise;
            },

            buildGetUrl:function(method){
                return endpoint.path + method;
            },

            /*
            * @method: getUserProfile
            * @params:
            *   - token: the aceess_token return by server
            *
            * @notes:
            *   The access token should be stored in the future to spotify service
            *   to keep the service running as its own entity.
             */
            getUserProfile:function(token){
                var method = 'me';
                var headers = {
                    Authorization: 'Bearer ' + token
                };

                return this.request(method, {}, headers, 'GET');
            },

            /*
            * @method: login
            * @description:
            *   Opens a javascript web browser and navigates to the
            *   spotify login page
             */
            login:function() {
                var d = $q.defer();
                var scope = spotify_scope;
                var state = apputils.generateRandomString(16);
                var authUrl = 'https://accounts.spotify.com/authorize?' +
                    apputils.serialize({
                        response_type: 'code',
                        client_id: client_id,
                        scope: scope,
                        redirect_uri: redirect_uri,
                        state: state
                    });

                console.log('Spotify Auth Url: ' + authUrl);
                var authWindow = window.open(authUrl, '_blank', 'location=no,toolbar=no');

                authWindow.addEventListener('loadstart', function(e){
                    console.log(JSON.stringify(e));

                    var url = e.url;

                    var code = /\?code=(.+)$/.exec(url);
                    var error = /\?error=(.+)$/.exec(url);

                    if(code || error){
                        authWindow.close();
                    }

                    if(code){
                        console.log(code);
                        var real_code = code[1].substring(0, code[1].indexOf('&'));
                        console.log(real_code);

                        var data = {
                            'grant_type':grant_type,
                            'code':real_code,
                            'redirect_uri':redirect_uri,
                            'client_id':client_id,
                            'client_secret':client_secret
                        };

                        $http({
                            url: 'https://accounts.spotify.com/api/token',
                            method:'POST',
                            data: data,
                            headers:{
                                'Content-Type':'application/x-www-form-urlencoded'
                            },
                            transformRequest:function(obj){
                                var str = [];
                                for(var p in obj)
                                    str.push(encodeURIComponent(p) + '=' + encodeURIComponent(obj[p]));
                                return str.join('&');
                            }
                        }).success(function(data){
                            d.resolve(data);
                        }).error(function(data){
                            console.log(data);
                            d.reject(data.responseJSON);
                        });
                    }else if(error){
                        d.reject({
                            error:error[1]
                        });
                    }
                });

                return d.promise;
            },

            refreshToken:function(){
                //TODO get a refresh token after token expires.
            }
        }
    }])
});