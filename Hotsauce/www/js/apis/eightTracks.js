define(['./module'],function(apis){
    apis.factory('eightTracks',['apputils', '$q', '$http', function(apputils, $q, $http){
        var api_key = '112b898e8d816c4c7f28bd247e7fdfd4c995d2d9';
        var default_params = {
            'api_key':api_key,
            'api_version':3,
            'format':'jsonp'
        };
        var headers = {
            'Content-Type':'application/json'
        };

        var endpoint = 'https://8tracks.com/';

        return {
            request:function(url, params, type){
                var d = $q.defer();
                $http({
                    url:url,
                    params:params,
                    header:headers,
                    type:type || 'GET'
                }).success(function(data){
                    d.resolve(data);
                }).error(function(data){
                    console.log('8tracks shit the bed.');
                    d.reject();
                });

                return d.promise;
            },

            login: function(username, password){
                var url = endpoint + 'sessions',
                    params = {
                        login: username,
                        password:password
                    };

                angular.extend(params, default_params);
                return this.request(url, params, 'POST');
            },

            login_web:function(){
                var d = $q.defer();
                var authUrl = endpoint + 'sessions?'+
                    apputils.serialize(default_params);
                var authWindow = window.open(authUrl, '_blank', 'location=no,toolbar=no');
                var execute = function(){
                    authWindow.executeScript({
                        code:"return document.cookie"
                    }, function(cookies){
                        console.log('ExecuteScript Results: ' + cookies);
                    });
                };
                authWindow.addEventListener('loadstop', function(e){
                    console.log(JSON.stringify(e));
                    execute();

                    /*
                    var url = e.url;
                    var token = /\#access_token=(.+)$/.exec(url);

                    if(token){
                        d.resolve(token);
                        authWindow.close();
                    }
                    */
                });

                return d.promise;
            },

            mix_sets:function(smart_id, options){
                var params = {},
                    url = endpoint + 'mix_sets/';
                if(options){
                    options.include? params.include = options.include : false;
                    options.sort? params.sort = options.sort: false;
                }else{
                    params.include = 'mixes';
                }
                if(smart_id){
                    url += smart_id;
                }else{
                    url += 'all';
                }

                angular.extend(params, default_params);
                return this.request(url, params);
            },

            showMix:function(id, user_token, options){
                var url = endpoint + 'mixes/'+id;
                var params = {};
                if(options){
                    params = options;
                }

                header['X-User-Token'] = user_token;
            }
        }
    }]);
})