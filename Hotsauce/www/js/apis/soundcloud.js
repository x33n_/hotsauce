define(['./module'], function(apis){
    'use strict';

    apis.factory('soundcloudApi', ['$http', '$q', 'apputils', function($http, $q ,apputils){
        var endpoint = {
            path:'https://api.soundcloud.com/'
        };

        var client_id="b0d27d94cc56dae0a32d46e8f0b58ed4";
        var client_secret="4cc5068b25102b4d691d799a39bd415a";
        var redirect_uri = "http://localhost";

        return{

            request:function(path, params, header, type){
                var schlep = $q.defer();

                var url = endpoint.path + path;
                $http({
                    url:url,
                    params:params,
                    headers:header,
                    method:type || 'GET'
                }).success(function(data){
                    schlep.resolve(data);
                }).error(function(e){
                    console.log('SOUNDCLOUD Error occurred: ' + JSON.stringify(e));
                });

                return schlep.promise;
            },

            favoriteTrack:function(userId, trackId, authKey, check){
                var path = 'users/'+userId+'/favorites/'+trackId,
                    params = {
                    oauth_token: authKey
                };

                if(check){
                    //TODO Check if a track is favorited already.
                }else{
                    return this.request(path, params, {}, 'PUT');
                }
            },

            login:function(){
                var d = $q.defer();
                var scope = 'non-expiring';
                var state = apputils.generateRandomString(16);
                var authUrl = endpoint.path + 'connect?' +
                    apputils.serialize({
                        client_id:client_id,
                        redirect_uri:redirect_uri,
                        response_type:'token',
                        scope:scope,
                        display:'popup',
                        state:state
                    });

                var authWindow = window.open(authUrl, '_blank', 'location=no,toolbar=no');

                authWindow.addEventListener('loadstart', function(e){
                    console.log(JSON.stringify(e));

                    var url = e.url;
                    var token = /\#access_token=(.+)$/.exec(url);

                    if(token){
                        d.resolve(token);
                        authWindow.close();
                    }
                });

                return d.promise;
            }
        };
    }]);
});