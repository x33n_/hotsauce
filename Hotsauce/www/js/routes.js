define(['./app'], function(app){
   'use strict';
    return app.config(['$routeProvider', function($routeProvider){
        $routeProvider.when('/search',{
            templateUrl: 'template/search-view.html',
            controller:'searchController'
        });

        $routeProvider.when('/profile', {
            templateUrl:'template/profile-view.html',
            controller:'profileController'
        });

        $routeProvider.when('/login', {
            templateUrl: 'template/login-view.html',
            controller:'loginController'
        });

        $routeProvider.when('/playlist', {
            templateUrl:'template/playlist-view.html',
            controller:'playlistController'
        });

        $routeProvider.when('/album', {
            templateUrl:'template/spotify-album.html',
            controller:'appCtrl'
        });

        $routeProvider.when('/play', {
            templateUrl:'template/player-view.html',
            controller:'profileController'
        });

        $routeProvider.otherwise({
            redirectTo:'/login'
        });
    }]);
});