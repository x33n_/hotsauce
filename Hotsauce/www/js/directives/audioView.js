define(['./module'], function(directives){
    'use strict';
    directives.directive('audioView', [function(){
        return{
            restrict:'E',
            replace:true,
            controller:function(musicPlayer, $scope, appScope, dataService, $timeout, soundcloudApi, navigator){
                $scope.audioPlayer = {
                    init:function(){
                        $scope.audioPlayer.largePlayer = navigator.getCurrentView() == '/play'? true : false;
                        $scope.audioPlayer.appScope = appScope.getScope();
                        $scope.audioPlayer.music = $scope.audioPlayer.appScope.music;
                        if($scope.audioPlayer.music) {
                            $scope.audioPlayer.songpaused = false;
                            $scope.audioPlayer.setTrackInfo();
                            if (!musicPlayer.playing()) {
                                $scope.audioPlayer.playpause();
                            }else if(musicPlayer.getCurrentSrc() != $scope.audioPlayer.current.name){
                                musicPlayer.pause();
                                $scope.audioPlayer.playpause();
                            }
                        }
                    },

                    setTrackInfo:function(){
                        $scope.audioPlayer.current = {
                            name:$scope.audioPlayer.music.tracks[$scope.audioPlayer.music.currentTrack].name,
                            image:$scope.audioPlayer.music.tracks[$scope.audioPlayer.music.currentTrack].image,
                            duration: $scope.audioPlayer.music.tracks[$scope.audioPlayer.music.currentTrack].duration,
                            description: $scope.audioPlayer.music.tracks[$scope.audioPlayer.music.currentTrack].description,
                            position:musicPlayer.getAudioPosition(),
                            liked:false
                        };
                        try{
                            $scope.$digest();
                        }catch(exception){
                            console.log('audioView: digest in effect. '+exception);
                        }
                    },

                    playpause:function(){
                        if(!musicPlayer.playing()){
                            if($scope.audioPlayer.songpaused){
                                musicPlayer.resume();
                                $scope.audioPlayer.timeout = $timeout($scope.audioPlayer.updateTime, 1000);
                                $scope.audioPlayer.songpaused = false;
                            }else {
                                var song = $scope.audioPlayer.music.tracks[$scope.audioPlayer.music.currentTrack].stream +
                                    $scope.audioPlayer.music.auth;
                                musicPlayer.play(
                                    song,
                                    $scope.audioPlayer.playSuccess,
                                    $scope.audioPlayer.playError,
                                    $scope.audioPlayer.current.name);
                                $scope.audioPlayer.songpaused = false;
                                $scope.audioPlayer.current.position = '0:00';
                                $scope.audioPlayer.timeout = $timeout($scope.audioPlayer.updateTime, 1000);
                            }
                        }else{
                            musicPlayer.pause();
                            $scope.audioPlayer.songpaused = true;
                            $timeout.cancel($scope.audioPlayer.timeout);
                        }
                    },

                    updateTime:function(){
                        $scope.audioPlayer.current.position = musicPlayer.getAudioPosition();
                        $scope.audioPlayer.timeout = $timeout($scope.audioPlayer.updateTime, 1000);
                    },

                    playSuccess:function(){
                        console.log('Hooray!');
                        $scope.audioPlayer.songpaused = false;
                        $scope.audioPlayer.next();
                    },

                    playError:function(error){
                        switch(error.code){
                            case MediaError.MEDIA_ERR_ABORTED:
                                console.log("AudioPlayer Error: ABORTED");
                                break;
                            case MediaError.MEDIA_ERR_NETWORK:
                                console.log("AudioPlayer Error: Network Error");
                                break;
                            case MediaError.MEDIA_ERR_DECODE:
                                console.log("AudioPlayer Error: Decode Error.");
                                break;
                            case MediaError.MEDIA_ERR_NONE_ACTIVE:
                                console.log("AudioPlayer Error: Not Active");
                                break;
                            case MediaError.MEDIA_ERR_NONE_SUPPORTED:
                                console.log("AudioPlayer Error: Not Supported.");
                                break;
                            case MediaError.MEDIA_ERR_SRC_NOT_SUPPORTED:
                                console.log("AudioPlayer Error: Source Not Supported.");
                                break;
                            default :
                                console.log("AudioPlayer Error: Unknown Error");
                        }

                        console.log(JSON.stringify($scope.audioPlayer.music.tracks[$scope.audioPlayer.music.currentTrack]));
                    },

                    next:function(){
                        musicPlayer.pause();
                        $scope.audioPlayer.music.currentTrack++;
                        appScope.set('music', {currentTrack: $scope.audioPlayer.music.currentTrack});
                        $scope.audioPlayer.setTrackInfo();
                        $scope.audioPlayer.songpaused = false;
                        $scope.audioPlayer.playpause();
                    },

                    previous:function(){
                        musicPlayer.pause();
                        $scope.audioPlayer.music.currentTrack--;
                        appScope.set('music', {currentTrack: $scope.audioPlayer.music.currentTrack});
                        $scope.audioPlayer.setTrackInfo();
                        $scope.audioPlayer.songpaused = false;
                        $scope.audioPlayer.playpause();
                    },

                    hasNext:function(){
                        return $scope.audioPlayer.music.currentTrack < $scope.audioPlayer.music.total - 1? false: true;
                    },

                    hasPrevious:function(){
                        return $scope.audioPlayer.music.currentTrack > 0? false: true;
                    },

                    goToAudioPlayer:function(){
                        navigator.navigate('/play');
                    },

                    favorite:function(){
                        if($scope.userProfile && !$scope.audioPlayer.current.liked){
                            var trackInfo = $scope.audioPlayer.music.tracks[$scope.audioPlayer.music.currentTrack].source,
                                authKey = appScope.get('soundcloudToken');

                            soundcloudApi.favoriteTrack($scope.userProfile.id, trackInfo.id, authKey.access_token, false).then(function(data){
                                console.log('Favorited Success');
                                $scope.audioPlayer.current.liked = true;
                            });
                        }
                    }
                };


                $scope.audioPlayer.init();
            },
            templateUrl:'js/directives/partials/audio-view.html'
        }
    }]);
});