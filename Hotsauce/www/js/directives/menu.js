define(['./module'], function(directives){
   'use strict';
    directives.directive('menu',['$rootScope',function($rootScope){
        return {
            restrict:'E',
            replace:true,
            controller:function($scope, dialogService, navigator, appScope){

                $scope.applicationScope = {};
                $scope.menu = {
                    opened:false,
                    available:false,
                    title:'CiSuM',

                    init:function(){
                        $scope.applicationScope = appScope.getScope();
                        $scope.menu.available = $scope.applicationScope.loggedIn;
                    },

                    open:function(){
                        if($scope.menu.opened){
                            $scope.menu.opened = false;
                        }else{
                            $scope.menu.opened = true;
                        }
                    },

                    services:function(){
                        $scope.menu.opened = false;
                        var dialogOptions = {
                            title:'Select a Service',
                            closeButtonText:'Cancel',
                            closeButtonAvailable:true
                        };

                        var dialogDefaults = {
                            templateUrl:'js/services/dialogs/serviceDialog.html'
                        };

                        dialogService.showModal(dialogDefaults, dialogOptions).then(function(result){
                            console.log('returned from popup. result: ' + result);
                            $scope.menu.setService(result);
                        });
                    },

                    setService:function(res){
                        switch(res){
                            case 1:
                                appScope.set('serviceCode', {value:1});
                                break;
                            case 2:
                                appScope.set('serviceCode', {value:2});
                                break;
                            case 3:
                                appScope.set('serviceCode', {value:3});
                                break;
                            case 4:
                                appScope.set('serviceCode', {value:4});
                                $scope.menu.toggle8Tracks(true);
                                break;
                            default:
                                $scope.title = 'Cisum';
                        }

                        if(res != 4){
                            $scope.menu.toggle8Tracks(false);
                        }
                    },

                    profile:function(){
                        //TODO Navigate to current profile
                        if($scope.applicationScope.loggedIn){
                            navigator.navigate('/profile');
                        }else{

                        }
                    },

                    search:function(){
                        navigator.navigate('/search');
                    },

                    favorites:function(){
                        //TODO
                    },

                    playlist:function() {
                        //TODO
                    },

                    radio:function(){
                        //TODO
                    },

                    recommendations:function(){
                        //TODO
                    },

                    popular:function(){
                        //TODO
                    },

                    toggle8Tracks:function(bool){
                        $rootScope.$broadcast('menu.eightTracks', bool);
                    }

                };

                $scope.menu.init();
            },

            templateUrl:'js/directives/partials/menu.html'
        }
    }]);
});