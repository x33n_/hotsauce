define(['./module'], function(directives){
   'use strict';
    directives.directive('playlistInfo', [function(){
        return{
            restrict:'E',
            replace:true,
            scope:{
                playlist:'='
            },
            templateUrl:'js/directives/partials/playlist_info.html'
        }
    }]);
});