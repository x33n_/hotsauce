define(['./module'], function(directives){
    'use strict';
    directives.directive('playlistTrack', [function(){
        return {
            restrict:'E',
            replace:true,
            scope:{
                track:'='
            },
            templateUrl:'js/directives/partials/playlist_track.html'
        }
    }]);
});