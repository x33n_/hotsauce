define(['./module'], function(directives){
    'use strict';
    directives.directive('footer', [function(){
        return{
            restrict: 'AE',
            replace:true,
            templateUrl:'js/directives/partials/footer.html',
            controller:function($scope, dataService, navigator){
                $scope.footer = {
                    available:false,

                    init:function(){
                        var map = dataService.get('navigation', 'map')
                        if(map){
                            if(map.length > 0){
                                $scope.footer.available = true;
                            }
                        }
                    },

                    goBack:function(){
                        navigator.goBack();
                    }

                };

                $scope.footer.init();
            }
        };
    }]);
});