define(['./module'], function(apis){
    'use strict';

    apis.factory('spotify', ['$http', '$q', function($http, $q){
        var endpoint = {
            path:"https://api.spotify.com/v1/"
        };
        return{
            /*
            ************************************
            *Main request for spotify API.
            ************* METHODS **************
            * @path: search
            * @param: q <required> Search's query words
            * @param:
             */
            request:function(method, params){
                var schlep = $q.defer();
                var url = this.buildGetUrl(method);
                $http.get(url, {params: params}).success(function(data){
                    schlep.resolve(data);
                });
                return schlep.promise;
            },

            buildGetUrl:function(method){
                return endpoint.path + method;
            }
        }
    }])
});