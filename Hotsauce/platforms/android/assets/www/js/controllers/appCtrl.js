/**
 * Created by benjamin on 6/10/14.
 */
define(['./module'
], function(controllers){
    'use strict';
    controllers.controller('appCtrl', ['$scope', '$rootScope', '$location', 'spotify', function($scope, $rootScope, $location, spotify){
        $scope.username = "";
        $scope.password = "";
        $scope.service = 'spotify';
        $scope.query = "";
        $scope.data = [];
        $scope.loadedAlbum = {};

        $scope.changeService = function(e){
            console.log(e);
        };

        $scope.getDuration = function(milliseconds){
            var ms = milliseconds,
                min = (ms/1000/60) << 0,
                sec = (ms/1000) % 60;

            return min + ":" + sec.toFixed();
        };

        //Watchers
        $scope.$watch('username', function(newValue){
            $scope.username = newValue;
        });

        $scope.$watch('password', function(nv){
            $scope.password = nv;
        });

        $scope.$watch('query', function(nv){
            $scope.query = nv;
        });
        //End Watchers

        //Button clicks
        $scope.login = function(username, password){
            var options = {
                username: username,
                password: password
            };
        };
        $scope.go = function(path){
            $location.path(path);
        };

        $scope.search = function(){
            if($scope.query !== ""){
                spotify.request('search', {q: $scope.query, type:'artist,album,track'}).then(function(result){
                    if(result){
                        if(result.artists){
                            $scope.data.artists = result.artists.items;
                        }
                        if(result.albums){
                            $scope.data.albums = result.albums.items;
                        }
                        if(result.tracks){
                            $scope.data.tracks = result.tracks.items;
                        }
                    }
                    $scope.loaded = true;
                });
            }
        };

        $scope.openAlbum = function(index){
            $scope.loadedAlbum = $scope.data.albums[index];
            $scope.go('album');
        };

        $scope.openSong = function(index){
            window.open($scope.data.tracks[index].href);
        };
        //End Buttons clicks
    }]);
});
