/**
 * Created by benjamin on 6/10/14.
 */
define(['./module'], function(services){
    'use strict';
    services.value('version', '0.1');
    services.value('spotifyUrl', 'https://api.spotify.com/v1/'); //todo Create a directive to serve to apis.
});
