define(['./app'], function(app){
   'use strict';
    return app.config(['$routeProvider', function($routeProvider){
        $routeProvider.when('/main',{
            templateUrl: 'templates/main-view.html',
            controller:'appCtrl'
        });

        $routeProvider.when('/login', {
            templateUrl: 'templates/login-view.html',
            controller:'appCtrl'
        });

        $routeProvider.when('/album', {
            templateUrl:'templates/spotify-album.html',
            controller:'appCtrl'
        });

        $routeProvider.otherwise({
            redirectTo:'/login'
        });
    }]);
});