define(['./module'], function(services){
    'use strict';
    /*
    *  Dialog service for popups.
    *  Creates a quick custom popup on the fly.
     */
    services.service('dialogService', ['$modal', function($modal) {
        var dialogDefaults = {
            templateUrl: 'js/services/dialogs/dialog.html',
            size:'sm'
        };

        var modalOptions = {
            closeButtonText: 'Close',
            okButtonText: 'OK',
            title: '',
            message: '',
            closeButtonAvailable: false
        };

        return{
            /*
            *  @method: showModal
            *  @params:
            *   - customDialogDefaults:
            *       @description: Overrides the default dialog options.
            *       @object: {
            *           templateUrl: <New URL for template>
            *        }
            *   - custommodalOptions:
            *       @description: Overrides the options for the default template
            *       @object: {
            *          closeButtonText: <Text for the cancel button defaults to 'cancel'>,
            *          okButtonText: <Text for the confirm button defaults to 'OK'>,
            *          title: <Header text for the popup>,
            *          message: <Message to display in popup>,
            *          closeButtonAvailable: <Boolean to display the cancel button or not>
            *
            *    @returns:
            *       - Returns a promise for the 'ok' button pressed.
             */
            showModal: function (customDialogDefaults, custommodalOptions) {
                if(!customDialogDefaults) customDialogDefaults = {};
                customDialogDefaults.backdrop = 'static';
                return this.show(customDialogDefaults, custommodalOptions);
            },

            /*
            *  This method functions the same as showModal and should not be used as it is called by showModal.
             */
            show: function (customDialogDefaults, custommodalOptions) {
                var tempModalDefaults = {};
                var tempModalOptions = {};

                angular.extend(tempModalDefaults, dialogDefaults, customDialogDefaults);
                angular.extend(tempModalOptions, modalOptions, custommodalOptions);

                if(!tempModalDefaults.controller){
                    tempModalDefaults.controller = function($scope, $modalInstance){
                        $scope.modalOptions = tempModalOptions;
                        $scope.modalOptions.ok = function(result){
                            $modalInstance.close(result);
                        };
                        $scope.modalOptions.close = function(result){
                            $modalInstance.dismiss('cancel');
                        };
                    }
                }

                return $modal.open(tempModalDefaults).result;
            }
        }
    }]);
});