define(['./module'], function(services){
   'use strict';
    services.service('appScope', [function(){
        var scope = {};
        return{
            add:function(key, value){
                scope[key] = value;
            },

            set:function(s){
                scope = angular.copy(scope, s);
            },

            get:function(key){
                return scope[key]? scope[key] : false;
            }
        }
    }]);
});