define(['./module'], function(directives){
    'use strict';
    directives.directive('audioView', [function(){
        return{
            restrict:'E',
            replace:true,
            controller:function(musicPlayer, $scope, appScope){

                $scope.init = function(){
                    //TODO init this beast
                    $scope.music = appScope.get('music');
                    $scope.playpause();
                };

                $scope.playpause = function(){
                    if(!musicPlayer.playing()){
                        var song = $scope.music.tracks[$scope.music.currentTrack].stream + $scope.music.auth;
                        musicPlayer.play(song, $scope.playSuccess, $scope.playFail);
                        $scope.songpaused = false;
                    }else{
                        musicPlayer.pause();
                        $scope.songpaused = true;
                    }
                };

                $scope.playSuccess = function(){
                    console.log('Hooray!');
                };

                $scope.playFail = function(err){
                    console.log("Error somewhere" + JSON.stringify(err));
                };

                $scope.next = function(){
                    if(musicPlayer.playing()){
                        musicPlayer.stop();
                    }
                    $scope.current++;
                    $scope.playpause();
                };

                $scope.previous = function(){
                    if($scope.current != 0){
                        if(musicPlayer.playing()){
                            musicPlayer.stop();
                        }
                        $scope.current--;
                        musicPlayer.playpause()
                    }
                };

                $scope.init();
            },
            templateUrl:'js/directives/partials/audio-view.html'
        }
    }]);
});