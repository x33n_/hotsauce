define(['./module'], function(directives){
   'use strict';
    directives.directive('menu', ['$rootScope', function($rootScope){
        return {
            restrict:'E',
            replace:true,
            controller:function($scope, dialogService, navigator, appScope){

                $scope.opened = false;
                $scope.title = 'Cisum';
                $rootScope.titleCode = 1;
                $scope.available = {
                    logged: !$rootScope.loggedIn
                };

                $scope.open = function(){
                    if($scope.opened){
                        $scope.opened = false;
                    }else{
                        $scope.opened = true;
                    }
                };

                $scope.services = function(){
                    $scope.opened = false;
                    var dialogOptions = {
                        title:'Select a Service',
                        closeButtonText:'Cancel',
                        closeButtonAvailable:true
                    };

                    var dialogDefaults = {
                        templateUrl:'js/services/dialogs/serviceDialog.html'
                    };

                    dialogService.showModal(dialogDefaults, dialogOptions).then(function(result){
                       console.log('returned from popup. result: ' + result);
                        $scope.setService(result);
                    });
                };

                $scope.setService = function(res){
                    switch(res){
                        case 1:
                            $scope.title = 'Spotify';
                            $rootScope.titleCode = res;
                            break;
                        case 2:
                            $scope.title = 'Soundcloud';
                            $rootScope.titleCode = res;
                            break;
                        case 3:
                            $scope.title = 'HypeM';
                            $rootScope.titleCode = res;
                            break;
                        case 4:
                            $scope.title = '8Tracks';
                            $rootScope.titleCode = res;
                            break;
                        default:
                            $scope.title = 'Cisum';
                            $rootScope.titleCode = 1;
                    }
                };

                $scope.profile = function(){
                    //TODO Navigate to current profile
                    if(!$scope.available.logged){
                        navigator.navigate('/profile');
                    }else{
                        dialogService.showModal({},
                            {
                                message:'Please login.',
                                title:'Not Logged in'
                            }).then(function(){
                                navigator.navigate('/');
                            });
                    }
                };

                $scope.favorites = function(){
                    //TODO Navigate to current favorites
                };

                $scope.playlist = function(){
                    //TODO Navigate to current playlist
                };

                $scope.radio = function(){
                    //TODO Navigate to current radio
                };

                $scope.recommendations = function(){
                    //TODO Navigate to recommendations
                };

                $scope.popular = function(){
                    //TODO Navigate to current popular
                };
            },

            templateUrl:'js/directives/partials/menu.html'
        }
    }]);
});