define([
    './app-version',
    './footer',
    './audioView',
    './menu',
    './playlistInfo',
    './playlistTrack',
    './image'
], function(){
});