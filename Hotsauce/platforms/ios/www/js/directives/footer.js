define(['./module'], function(directives){
    'use strict';
    directives.directive('footer', [function(){
        return{
            restrict: 'AE',
            replace:true,
            templateUrl:'js/directives/partials/footer.html'
        };
    }]);
});