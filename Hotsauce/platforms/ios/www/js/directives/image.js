define(['./module'], function(directives){
    'use strict';
    directives.directive('image', function(){
        return function(scope, element, attrs){
            var url = attrs.image;
            element.css({
                'background-image':'url('+ url +')',
                'background-size':'cover',
                'opacity':0.5
            });
        };
    });
});