/**
 * Created by benjamin on 7/18/14.
 */
define(['./module'], function(factories){
    'use strict';
    factories.factory('musicPlayer', ['$rootScope', function(rootScope){
        var isPlaying = false;
        var currentPlayer = {};
        var mediaTimer = null;

        rootScope.audioPosition = '';

        return{
            playing:function(){
              return isPlaying;
            },

            play:function(src, success, fail){
                currentPlayer = new Media(src, success, fail);

                currentPlayer.play();
                isPlaying = true;

                if(mediaTimer == null){
                    var that = this;
                    mediaTimer = setInterval(function(){
                        currentPlayer.getCurrentPosition(
                            function(position){
                                if(position > -1){
                                    that.setAudioPosition((position) + "sec");
                                }
                            },

                            function(e){
                                console.log('Media player broke :(');
                                that.setAudioPosition("Error: " + e);
                            }
                        );
                    }, 1000);
                }
            },

            pause:function(){
                if(currentPlayer){
                    currentPlayer.pause();
                    isPlaying = false;
                }
            },

            stop:function(){
                if(currentPlayer){
                    currentPlayer.stop();
                    isPlaying = false;
                }
                clearInterval(mediaTimer);
                mediaTimer = null;
            },

            setAudioPosition:function(position){
                rootScope.audioPosition = position;
            },

            getAudioPosition:function(){
                return rootScope.audioPosition;
            }
        }
    }]);
});
