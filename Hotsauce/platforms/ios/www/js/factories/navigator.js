define(['./module'], function(factories){
    'use strict';
    factories.factory('navigator', ['$rootScope', '$location', function(scope, location){

        var previousPage = '';

        return{
            /*
             * Navigator to take browser to the specified path and trigger the emit event
             * To pass data between controllers.
             */
            navigate:function(route, event){
                previousPage = location.path();
                location.path(route);
                if(event){
                    //TODO maybe events?
                }
            },

            /*
            * Give some very simple go back functionality to the app.
             */
            goBack:function(){
                location.path(previousPage);
            }
        }
    }]);
});