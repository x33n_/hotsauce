/**
 * Created by benjamin on 7/17/14.
 */
define(['./module'], function(controllers){
    'use strict';
    controllers.controller('profileController',
        [
            '$scope',
            '$rootScope',
            'appScope',
            'navigator',
            'spotify',
            'soundcloudApi',
            'dataService',
            function(scope, rootScope, appScope, navigator, spotify, sc, cisumData){
                scope.userProfile = {};
                scope.tokenObject = {};
                scope.displayInfo = {};
                scope.loaded = false;
                scope.picturePath = '';
                scope.id = rootScope.id;

                console.log('id = ' + scope.id);

                scope.init = function(){
                    var rawUser = cisumData.get(scope.id, 'userProfile');
                    scope.tokenObject = cisumData.get(scope.id, 'token');
                    scope.userProfile = cisumData.createUserProfile(scope.id, rawUser);

                    if(scope.id === 'soundcloud'){
                        getSoundCloudFavorites();
                    }else if(scope.id === 'spotify'){
                        //TODO Make a method to get spotify playlist info for default.
                    }
                };

                scope.playSong = function($index){
                    var track ={
                        tracks:scope.displayInfo.info.tracks,
                        auth:'?oauth_token='+scope.tokenObject.access_token,
                        currentTrack: $index,
                        total:scope.displayInfo.info.tracks.length
                    };

                    appScope.add('music', track);
                    navigator.navigate('/play');
                };

                var getSpotifyPlaylist = function(){
                    var at = scope.tokenObject.access_token,
                        user_id = scope.userProfile.source.id,
                        path = 'users/'+user_id+'/playlist';

                };

                var getSoundCloudFavorites = function(){
                    var at = scope.tokenObject.access_token,
                        user_id = scope.userProfile.source.id,
                        path = 'users/'+user_id+'/favorites.json';

                    sc.request(path, {oauth_token:at},{},'GET').then(function(data){
                        cisumData.set('soundcloud', 'userFavorites', data);
                        scope.displayInfo.info = cisumData.createTrackInformation('soundcloud', data);
                        scope.loaded = true;
                    });
                };

                scope.init();
            }
        ]
    );
});