/**
 * Created by benjamin on 7/17/14.
 */
define(['./module'],
    function (controllers) {
        "use strict";
        controllers.controller('loginController',
            [
                'navigator',
                '$scope',
                '$rootScope',
                'appScope',
                'spotify',
                'apputils',
                'dataService',
                'dialogService',
                'soundcloudApi',
                function (navigator, scope, $rootScope, appScope, spotify, apputils, cisumData , dialog, sc) {

                    scope.login = function () {
                        //The following will be a reference table for services.
                        // 1 - <Default> Spotify
                        // 2 - Soundcloud
                        // 3 - HypeM
                        // 4 - 8Tracks
                        // More to be added later.

                        var title = $rootScope.titleCode;
                        $rootScope.loggedIn = false;
                        $rootScope.id = 'spotify';

                        switch(title){
                            case 1:
                                $rootScope.id = 'spotify';
                                spotifyLogin();
                                break;
                            case 2:
                                $rootScope.id = 'soundcloud';
                                soundCloudLogin();
                                break;
                            case 3:
                                dialog.showModal({},
                                    {
                                        title: 'Not Implemented',
                                        message:'HyepM not implemented :(',
                                        closeButtonAvailable: false
                                    });
                                //HypeM Login();
                                break;
                            case 4:
                                //8tracks
                                dialog.showModal({},
                                    {
                                        title: 'Not Implemented',
                                        message:'8Tracks not implemented :(',
                                        closeButtonAvailable: false
                                    });
                                break;
                            default:
                                console.log('Do nothing.');
                        }
                    };

                    //Spotify Login method
                    var spotifyLogin = function(){
                        spotify.login().then(function (data) {
                            console.log("loginController received Access Token");
                            var token = apputils.queryStringToJson(data[1]);
                            cisumData.set('soptify', 'token', token); //TODO Hardcode spotify from constants.
                            if(token.access_token){
                                spotify.getUserProfile(token.access_token).then(function(data){
                                    console.log('User profile received');
                                    cisumData.set('spotify', "userProfile", data); //TODO Hardcode spotify from constants.
                                    //Go to profile.
                                    $rootScope.loggedIn = true;
                                    navigator.navigate('/profile');
                                });
                            }
                        }, function (err) {
                            console.log('No Token Received Error' + err);
                            dialog.showModal({},
                                {
                                    title:'Error',
                                    message:'Could not connect to service.',
                                    closeButtonAvailable: false
                                });
                        });
                    };

                    //Soundcloud login
                    var soundCloudLogin = function(){
                        sc.login().then(function(data){
                            console.log('Soundcloud Access Token Retrieved DATA: ' + JSON.stringify(data));
                            var token = apputils.queryStringToJson(data[1]);
                            cisumData.set('soundcloud', 'token', token); //TODO Hardcode soundcloud in constants.
                            if(token.access_token){
                                sc.request('me.json',{"oauth_token":token.access_token}, {}, 'GET').then(function(data){
                                    console.log('User Profile Retrieved from soundcloud! ' + JSON.stringify(data));
                                    cisumData.set('soundcloud','userProfile', data);
                                    navigator.navigate('/profile');
                                });
                            }
                        });
                    }
                }
            ]
        );
    });