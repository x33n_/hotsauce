/**
 * Created by benjamin on 6/10/14.
 */
define([
    'angular',
    'angular-route',
    'angular-touch',
    'angular-ui',
    './controllers/index',
    './directives/index',
    './filters/index',
    './services/index',
    './apis/index',
    './factories/index'
], function(ng){
    'use strict';

    return ng.module('app',[
        'app.controllers',
        'app.directives',
        'app.filters',
        'app.services',
        'app.apis',
        'app.factories',
        'ngRoute',
        'ngTouch',
        'ui.bootstrap'
    ]);
});