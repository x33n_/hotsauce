/**
 * Created by benjamin on 6/10/14.
 */
require.config({

    paths:{
        'angular':'../lib/angular/angular',
        'angular-route':'../lib/angular-route/angular-route',
        'angular-touch':'../lib/angular-touch/angular-touch',
        'angular-ui':'../lib/angular-bootstrap/ui-bootstrap',
        'angular-ui-tpls':'../lib/angular-bootstrap/ui-bootstrap-tpls',
        'domReady':'../lib/requirejs-domready/domReady',
        'soundcloud':'../lib/soundcloud/sdk'
    },

    shim:{
        'angular':{
            exports:'angular'
        },
        'angular-route':{
            deps:['angular']
        },
        'angular-touch':{
            deps:['angular']
        },
        'angular-ui':{
            deps:['angular']
        },
        'angular-ui-tpls':{
            deps:['angular', 'angular-ui']
        }
    },

    deps:['./bootstrap']
});
